package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class MinMaxTest
{
	@Test
	public void Maxtest() throws Exception
	{
		MinMax minmax =  new MinMax();
		assertEquals(10, minmax.find(10, 2));
	}
	
	@Test
	public void MaxEqualtest() throws Exception
	{
		MinMax minmax =  new MinMax();
		assertEquals(10, minmax.find(10, 10));
	}

	@Test
	public void MaxNottest() throws Exception
	{
		MinMax minmax =  new MinMax();
		assertNotEquals(2, minmax.find(10, 2));
	}
}
